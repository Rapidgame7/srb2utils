local function rotatePoint(px, py, angle, cx, cy)
	-- x y are the coords to rotate
	-- cx and cy are the points to rotate around of, if available
	local s,c = sin(angle),cos(angle)
	
	cx = $ or 0
	cy = $ or 0
	
	px = $-cx
	py = $-cy
	
	local xnew = FixedMul(px, c) - FixedMul(py, s)
	local ynew = FixedMul(px, s) + FixedMul(py, c)
	
	px = xnew + cx
	py = ynew + cy
	
	return px,py
end