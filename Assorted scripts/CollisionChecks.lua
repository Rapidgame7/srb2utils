--- Checks if two objects intersect vertically.
-- Params: mobj_t m, mobj_t o
-- Returns: bool areTheyColliding
local function ZCollisionCheck(m, o)
	return m.z < o.z+o.height
	   and o.z < m.z+m.height
end

/* -- useless?
local function CollisionCheck(ms, ml, ns, nl)
	return ms < ns+nl and ns < ms+ml
end

local function RadiusCollisionCheck(m, o)
	return m.x-m.radius < o.x+o.radius
	   and m.x+m.radius > o.x-o.radius
	   and m.y-m.radius < o.y+o.radius
		 and m.y+m.radius > o.y-o.radius
end
*/

rawset(_G, "ZCollisionCheck", ZCollisionCheck)