-- Select a random number index from a table
local function randomChoose(t)
	if type(t) ~= "table" then error("this takes a table",2) end
	
	if #t == 0 then return nil end
	if #t == 1 then return t[1] end
	
	return t[ P_RandomRange(1,#t) ]
end

-- Shuffles all numerical indexes
local function shuffleTable(t)
	if type(t) ~= "table" then error("this takes a table",2) end
	
	for i = #t, 2, -1 do
		local j = P_RandomRange(1,i)
		t[i], t[j] = t[j], t[i]
	end
	
	return t
end