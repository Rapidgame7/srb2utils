local FRACUNIT = FRACUNIT
local AngleFixed,FixedAngle = AngleFixed,FixedAngle

-- Returns the difference between angle_ts a and b
-- as FRACUNITs. (Basically, sum result to a to reach b)
-- If giveAngle is true, converts result to angle_t.
local function angleDiff(a, b, giveAngle)
	a,b = AngleFixed($1),AngleFixed($2)
	
	local diff = b - a
	
	while diff >   180*FRACUNIT do diff = $ - 360*FRACUNIT end
	while diff <= -180*FRACUNIT do diff = $ + 360*FRACUNIT end
	
	if giveAngle then diff = FixedAngle(diff) end
	return diff
end