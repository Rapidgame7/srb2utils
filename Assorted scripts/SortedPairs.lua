-- sorted pairs but cooler

local function spairs(t, sortfun)
	local keys = {}
	for k in pairs(t) do keys[#keys+1] = k end -- collect keys
	
	table.sort(keys, sortfun) -- sort
	
	local i = 0
	return function() -- iterate
		i = $+1
		if keys[i] then return keys[i],t[keys[i]] end
	end
end

rawset(_G, "spairs", spairs)