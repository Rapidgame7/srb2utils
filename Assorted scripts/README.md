# Assorted scripts!
This folder contains small scripts, the kind you'd usually implement yourself once and reuse through the entire mod.

|File|Description|
|:-:|:-|
|`AngleDiff.lua`|Get the difference betwen two angles.|
|`ClosestPointOnLineBound.lua`| A `P_ClosestPointOnLine` that stops at the vertices of a line.|
|`CollisionChecks.lua`|Stuff regarding collision checks (for MobjCollide, really).|
|`DrawContents.lua`|Debug function to recursively display the contents of a table.|
|`Polar2Cartesian.lua`|Turn polar coordinates [angle,radius] into cartesian ones [x,y]. Has a 3D version too.|
|`Randomization.lua`|A bunch of functions regarding randomization, like `randomChoose` and `shuffleTable`.|
|`Random_VoseAlias.lua`|An elaborate implementation of Vose's Alias method for efficient sampling of random values, tweaked to use fixed point.|
|`RotatePoint.lua`|Rotate a point an amount of angles, optionally with a specific anchor point.|
|`SortedPairs.lua`|`pairs` but sorted.|
|`ValueManipulation.lua`|Snippets regarding value manipulation. Stuff like wrapping around two bounds, clamping, getting a distance...|
