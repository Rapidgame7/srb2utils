-- make some globals local for optimization
local type = type
local numType = "number"
local cos,sin = cos,sin
local FixedMul = FixedMul

--- Translates 2D polar coordinates to cartesian coordinates.
-- Takes an aim angle (hAngle) and a distance (radius),
-- and outputs its X and Y components.
--
-- Params: angle_t hAngle, fixed_t radius, [fixed_t scale]
-- Returns: fixed_t xFactor, fixed_t yFactor
local function Polar2Cartesian(hAngle, radius, scale)
	if type(hAngle) ~= numType then error("bad argument #1: expected number, got "..type(hAngle), 2) end
	if type(radius) ~= numType then error("bad argument #2: expected number, got "..type(radius), 2) end
	
	if scale ~= nil then
		if type(scale) ~= numType then
			error("bad argument #3: expected number or nil, got "..type(scale), 2)
		end
		radius = FixedMul(radius, scale)
	end
	
	return FixedMul( cos(hAngle), radius ), -- xFactor
	       FixedMul( sin(hAngle), radius )  -- yFactor
end

--- Translates 3D polar coordinates to cartesian coordinates.
-- Takes two aim angles (hAngle) (vAngle) and a distance (radius),
-- and outputs its X, Y and Z components.
--
-- Params: angle_t hAngle, angle_t vAngle, fixed_t radius, [fixed_t scale]
-- Returns: fixed_t xFactor, fixed_t yFactor, fixed_t zFactor
local function Polar2Cartesian3D(hAngle, vAngle, radius, scale)
	if type(hAngle) ~= numType then error("bad argument #1: expected number, got "..type(hAngle), 2) end
	if type(vAngle) ~= numType then error("bad argument #2: expected number, got "..type(vAngle), 2) end
	if type(radius) ~= numType then error("bad argument #3: expected number, got "..type(radius), 2) end
	
	if scale ~= nil then
		if type(scale) ~= numType then
			error("bad argument #4: expected number or nil, got "..type(scale), 2)
		end
		radius = FixedMul(radius, scale)
	end
	
	local cosh,cosv = cos(hAngle),cos(vAngle)
	local sinh,sinv = sin(hAngle),sin(vAngle)
	return FixedMul( FixedMul( radius, cosh ), cosv ), -- xFactor
	       FixedMul( FixedMul( radius, sinh ), cosv ), -- yFactor
	       FixedMul( radius, sinv ) -- zFactor
end

-- long
rawset(_G, "Polar2Cartesian", Polar2Cartesian)
rawset(_G, "Polar2Cartesian3D", Polar2Cartesian3D)
-- legacy
rawset(_G, "p2c", Polar2Cartesian)
rawset(_G, "p2c3d", Polar2Cartesian3D)
rawset(_G, "cSpd", Polar2Cartesian)
rawset(_G, "cSpdEx", Polar2Cartesian3D)







