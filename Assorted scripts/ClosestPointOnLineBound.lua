-- Like P_ClosestPointOnLine
-- but this one stops at the vertexes...
local function ClosestPointOnLineBound(x, y, line)
	local px,py = P_ClosestPointOnLine(x, y, line)
	local v1,v2 = line.v1,line.v2
	
	local vxMin,vxMax = v1.x, v2.x
	local vyMin,vyMax = v1.y, v2.y
	if vxMin > vxMax then vxMin,vxMax = vxMax,vxMin end
	if vyMin > vyMax then vyMin,vyMax = vyMax,vyMin end
	
	return min(max(px, vxMin), vxMax),
	       min(max(py, vyMin), vyMax)
end

-- some like it with the P_ prefix
rawset(_G, "P_ClosestPointOnLineBound", ClosestPointOnLineBound)