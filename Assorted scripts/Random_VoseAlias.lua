-- SRB2 BLua implementation of Michael Vose's Alias method
-- for sampling random values from a discrete probability distribution.
-- With help from Keith Schwarz's site's visualizations and explanations
-- https://www.keithschwarz.com/darts-dice-coins/
-- (and a little cheating by looking at Java code and figuring out where to go).
-- https://www.keithschwarz.com/interesting/code/?dir=alias-method

-- AKA: weighted RNG for SRB2 by amperbee
-- tweaked for (my) readability thankyouverymuch,
-- and for use with fixed point numbers.

/* INTENDED USAGE:
Instantiate a vosealias object through `vosealias(options)`.
Generate a random result through the instance's `roll()` method.

FUNCTIONS:

	vosealias(options)
(Implicit: Calls `vosealias:init(options)`)
Instantiate a vosealias object and initialize it.
Returns the new object.

	vosealias:init(options)
Initialize this vosealias object.
`options` must follow the following format:
options = {
	{weight, result},
	{weight, result},
	{weight, result},
	...,
}
where `weight` is a fixed point number representing a probability for this result,
and `result` is *any* value to be returned if this is rolled.
Normally you don't need to call this - the constructor does it automatically.

	vosealias:roll()
Rolls a random number using the weighted distribution given during initialization.

*/

-- go go gadget save micros
local FixedDiv = FixedDiv
local FRACUNIT = FRACUNIT
local setmetatable = setmetatable
local P_RandomRange = P_RandomRange
local P_RandomFixed = P_RandomFixed

local vosealias = {}

function vosealias:init(options)
	-- TODO sanitize
	if self == vosealias then error("instance me first!",2) end
	--inputcoins = $ or self.inputcoins
	
	local count = #options -- amount of options
	if count == 0 then error("no options available?",2) end
	
	local labels = {}
	local weights = {}
	
	local sum = 0 --sum of all weights
	for i = 1,count do
		local opt = options[i]
		local wt,lb = opt[1],opt[2]
		weights[i],labels[i] = wt,lb
		--opt[1] = $ * FRACUNIT -- we lack decimals, so use fixed point
		sum = $ + wt
	end
	
	-- calculate average probability
	--local average = FixedDiv(FRACUNIT, count)
	--local average = FixedDiv(sum, count)
	-- turns out we don't need to do this lol
	
	-- normalize weights, then sort
	local smallWork,largeWork = {},{}
	for i = 1,count do
		local wei = weights[i]
		wei = FixedDiv(wei, sum) * count
		
		-- put index of option in appropiate table
		if wei >= FRACUNIT then
			largeWork[#largeWork+1] = i
			--print( ("%d LARGE %.3f"):format(i, wei) )
		else
			smallWork[#smallWork+1] = i
			--print( ("%d SMALL %.3f"):format(i, wei) )
		end
		
		weights[i] = wei
	end
	
	-- arrays with configurations for the biased coinflips:
	-- chance of heads based on probs[i]
	local probs = {} -- on heads, return i
	local alias = {} -- on tails, return alias[i] instead. LOSER
	while #largeWork > 0 and #smallWork > 0 do
		local large = largeWork[#largeWork]
		local small = smallWork[#smallWork]
		smallWork[#smallWork] = nil -- we're never using this again!!!
		
		local smallWei = weights[small]
		local largeWei = weights[large]
		--print( ("%d %.3f > %d %.3f"):format(large, largeWei, small, smallWei) )
		
		probs[small] = smallWei -- probability of cointoss winning
		alias[small] = large -- my nemesis
		
		largeWei = $ + smallWei - FRACUNIT -- chop off some chance
		weights[large] = largeWei
		--print( ("%d %.3f !"):format(large, largeWei) )
		if largeWei < FRACUNIT then
			smallWork[#smallWork+1] = large -- DEMOTED
			largeWork[#largeWork] = nil
		end
	end
	
	-- either work list is empty, so mathematically they should be probability 1
	for i = 1,#largeWork do
		--print( ("%d LADJ"):format(i) )
		probs[largeWork[i]] = FRACUNIT
	end
	for i = 1,#smallWork do -- should not happen, but rounding issues are inevitable
		--print( ("%d SADJ"):format(i) )
		probs[smallWork[i]] = FRACUNIT
	end
	
	-- we're done here!
	
	self.labels = labels
	self.weights = weights -- useless tbh
	self.count = count
	
	self.probs = probs
	self.alias = alias
	
	/*
	for i = 1,count do
		print( ("PROB FOR %s IS %.3f OR GET %s"):format(
			tostring(labels[i]),
			probs[i],
			tostring(labels[alias[i]])
		) )
	end
	*/
	
	return self
end

function vosealias:roll()
	local column = P_RandomRange(1,self.count)
	local nemesis = self.alias[column]
	
	local prob = self.probs[column]
	local cointoss = P_RandomFixed() < prob
	
	local labels = self.labels
	if cointoss then return labels[column]
	else return labels[nemesis] end
end

local aliasmeta = {__index = vosealias}
function vosealias:instance(options)
	local instance = setmetatable({}, aliasmeta)
	local res = instance:init(options)
	return res
end

local rootaliasmeta = {
	--__index = vosealias -- VERY bad idea
	__call = vosealias.instance,
}
setmetatable(vosealias, rootaliasmeta)


rawset(_G, "vosealias", vosealias)

