-- Clamps value to vMin and vMax
-- A bit useless since there's min and max available.
local function valClamp(value, vMin, vMax)
	if vMin > vMax then
		vMin,vMax = vMax,vMin
	end
	local hasClamp = false
	if value < vMin then value = vMin;hasClamp = true end
	if value > vMax then value = vMax;hasClamp = true end
	return value, hasClamp
end

-- Wrap value around if it surpasses either bounds, inclusive.
local function valWrap(value, vMin, vMax)
	if value == nil then error("#1 nil", 2) end
	if vMin == nil then error("#2 nil", 2) end
	if vMax == nil then error("#3 nil", 2) end
	
	if vMin > vMax then
		vMin,vMax = vMax,vMin
	end
	
	local dist = abs(vMin - vMax)+1
	while value > vMax do value = value - dist end
	while value < vMin do value = value + dist end
	return value
end

-- Divides by this much and returns two values
local function valSplit(value, divisor)
	local result = value/divisor
	return result,value-result
end

-- If value is within epsilon of target, return target
-- Otherwise, return value
local function valEpsilon(value, target, epsilon)
	if value > target - epsilon
	and value < target + epsilon
		then return target
	else return value end
end

-- Returns numerical distance between two values
local function valDist(v1, v2)
	return abs(v1 - v2)
end

-- Returns sign of value
-- Optionally, gets sign of value relative to another
local function valSign(value, center)
	center = center or 0
	if n > center then return 1
	elseif n < center then return -1
	else return 0 end
end

-- Dynamically add or substract step to value to move towards target.
-- Does not overshoot, but can be forced to.
local function valApproach(value, target, step, overshoot)
	if step == 0 then return value end -- lol
	if value == target then return value end -- we're already here!
	
	local dist = target - value
	
	if overshoot ~= true and abs(dist) < abs(step) then -- this step takes us to target
		return target -- at target
	end
	-- we overshoot or we're not there yet, so:
	
	local distsign = dist > 0 and 1 or -1
	return value + step * distsign
end










