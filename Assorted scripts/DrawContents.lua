local dict = {
	["nil"]      = {name="nil"   , address=false}, -- normally unseen
	
	["boolean"]  = {name="bool"  , address=false},
	["number"]   = {name="int"   , address=false},
	["string"]   = {name="str"   , address=false},
	
	["function"] = {name="func"  , address=true },
	["userdata"] = {name="udata" , address=true ,isudata = true},
	["table"]    = {name="table" , address=true ,istable = true},
	
	["thread"]   = {name="thread", address=true }, -- lol rare
}

local tblIterator = spairs or pairs -- prefer spairs, it's just better
local state = {}

local function writethis(txt)
	state.drawer.drawString(state.x + state.level*4, state.y,
		txt,
	V_ALLOWLOWERCASE, "small-thin")
	
	state.y = $+4
end

local function internal_drawcontents(tbl)
	if type(tbl) ~= "table" then error("This is not a table!",2) end
	if next(tbl) == nil then -- table is empty
		writethis("\134".."[empty]")
		return
	end
	if tbl._HIDE then -- table is hidden (spoilers, too long, other)
		writethis("\134".."[hidden]")
		return
	end
	for k,v in tblIterator(tbl) do
		local vstr,vtype = tostring(v),type(v)
		local entry = dict[vtype]
		
		local part_type,part_key,part_value =
			entry.name,
			tostring(k), -- do something else? this works for now
			vstr
		;
		
		if entry.address then
			local hex = vstr:sub(-8,-1) -- might not work right in x64
			part_value = hex
			if entry.isudata then
				local utype = userdataType(v)
				part_value = utype.." "..$
			end
			if entry.istable then
				part_type = $.."["..#v.."]"
			end
		end
		
		writethis( ("\130%s \128%s \131%s"):format(part_type,part_key,part_value) )
		
		if entry.istable then
			state.level = $+1
			internal_drawcontents(v)
			state.level = $-1
		end
		
	end
end


rawset(_G, "DrawContents", function(dw, x, y, tbl)
	-- Draws table `tbl` recursively.
	-- Takes a drawer dw and int coordinates x and y
	
	state.level = 0
	state.x = x
	state.y = y
	state.drawer = dw
	
	if type(tbl) ~= "table" then
		local ok = tbl
		tbl = {
			["THIS IS NOT A TABLE!!!"] = true,
			["value"] = ok
		}
	end -- not ideal but gets attention
	
	internal_drawcontents(tbl)
end)