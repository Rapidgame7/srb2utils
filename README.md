Reinventing the wheel is annoying, so instead of having to investigate implementations or dig through old mods everytime you want to do something you've already done, why not stash them in a repo of scripts for later access?

# srb2utils

This repository contains a few projects or some assorted small scripts that can be used to add flavor to a mod or implement specific behavior.

Random scripts are found under the `Assorted scripts` folder. They aren't big enough to warrant their own project folder, lol.  
Larger projects will have their own, and may include their own `README.md`.

|Folder|Description|
|:-:|-|
|`TextWriter`|An elaborate attempt at a custom string drawer that accepts custom fonts and extra formatting options.|

More to come. Don't stay tuned, I don't have a schedule lmao.

---
should probably rename the repo

