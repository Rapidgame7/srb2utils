font structure = {
	-- the prefix to use when caching glyph graphics into patches
	-- should be in the format XXXXXYYY, where
	-- XXXXX is a 5 character long prefix, and YYY is a 0-padded number representing ascii code
	-- used by sentinel to know what patches to cache
	prefix = string,
	
	-- the width of the space character
	spacewidth = int, -- default: half of monowidth
	
	-- the width of each glyph and whitespace under monospace rules
	monowidth = int, -- default: the width of the widest glyph available

	-- amount of bonus space between each drawn glyph
	sepwidth = int,

	-- all glyphs available in font
	-- created and used internally
	glyphs = table,
}

-------

/*
drawdata = {
	-- any parameter with no default will cause an error if missing
	
	-- starting corner for drawing
	x = fixedint,
	y = fixedint,
	
	-- font to use
	font = string,
	
	-- string to draw
	text = string,
	
	-- flags for drawing. this is passed to v.drawStretched directly
	flags = int, -- default: 0
	
	-- how big glyphs should be
	-- if hscale or vscale are missing, scale is used in its place
	scale = fixedint, -- default: FRACUNIT
	hscale = fixedint, -- default: scale
	vscale = fixedint, -- default: scale
	
	-- alignment of each line, practically
	-- a value of -FU will draw the leftmost side of each line at x (left align)
	-- a value of 0 will try to center each line at x (center align)
	-- a value of FU will draw the rightmost side of each line at x (right align)
	-- analogous meaning for y, except this is for the combo of all lines
	halign = fixedint, -- default: -FRACUNIT
	valign = fixedint, -- default: -FRACUNIT
	
	-- colorization of glyphs
	-- if color is set, drawer will get a colormap out of it
	-- and store it in colormap
	color = int, -- default: nil
	colormap = colormap, -- default: nil
}

-- shorthand is supported for some keys
drawdata = {
	[1] = x,
	[2] = y,
	[3] = font,
	[4] = text,
	[5] = flags,
}


*/

-------

/*
	ALIGN [-FRACUNIT, FRACUNIT]
				-1                  1
	
	IF BOARD WIDTH IS 100
	a line of 60 width should have an x of:
	for left align, 0 (mul:0)
	for center align, 20 (mul:0.5)
	for right align, 40 (mul:1)
	
	linealign = (100 - 60) * ((ALIGN+1)/2)
	
	a board of 100 width should have an x of:
	for left align, 0 (mul:0)
	for center align, -50 (mul:-0.5)
	for right align, -100 (mul:-1)
*/

	/* L201 makeBoard refactorization
	
	what if i split the string into pieces first
	(calculating width and color and other)
	before placing the pieces in their respective lines and doing post calcs?
	
	may make some calculations and comparisons more efficient
	may improve code readability
	(EG: precalculate word lengths to make word wrapping smoother)
	(OR lookahead, sum next pieces to figure out word length)
	(EG: special pieces (whitespace, color codes, tabulation, uhhhhh) may be handled better)
	(other?)
	*/