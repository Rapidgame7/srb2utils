-- incomplete
local WTXT = {}

local function devprint(...)
	if not devparm then return end
	local argcount = select('#', ...)
	local argtbl = {...}
	local bigstr = ""
	for argix = 1,argcount do
		local arg = argtbl[argix]
		bigstr = $ .. tostring(arg) .. (argix ~= argcount and "\t" or "")
	end
	print("\139[WTXT]\128\t" .. bigstr)
end
local function devprint_unpaused(...)
	if paused then return end
	devprint(...)
end

local fonts = {}

local function buildFont(v, font)
	devprint("buildFont called...", font.name)
	-- (re)builds a font and its glyphs
	-- should normally be done only once, on font creation
	-- assumes font is good!!! issues should be handled by createNewFont
	
	local glyphs = {}
	
	local glyphExistCount = 0
	local maxWidth = 0
	local maxHeight = 0
	
	for ascii = 0,255 do -- for each possible glyph in a font
		local glyph = {}
		
		local patchName = string.format("%s%03d", font.prefix, ascii)
		glyph.exists = v.patchExists(patchName)
		
		if glyph.exists then
			glyphExistCount = $+1
			local patch = v.cachePatch(patchName)
			glyph.patch = patch
			glyph.width = patch.width
			glyph.height = patch.height
			
			maxHeight = max($, patch.height)
			maxWidth = max($, patch.width)
		end
		
		-- no more editing past this point
		--glyphs[ascii] = setmetatable(glyph, _frozen)
		glyphs[ascii] = glyph
	end
	
	--local avgWidth =
	
	-- some other font work
	-- we can afford to be slow and precise. this is run once
	font.validGlyphs = glyphExistCount
	font.maxWidth = maxWidth
	font.maxHeight = maxHeight
	
	if font.sepwidth == nil then
		devprint("sepwidth missing, set to 0")
		font.sepwidth = 0
	end
	
	if font.monowidth == nil then
		devprint("monowidth missing, using widest glyph's width", maxWidth)
		font.monowidth = maxWidth
	end
	
	if font.spacewidth == nil then
		local half = font.monowidth / 2
		devprint("spacewidth missing, using half of monowidth", font.monowidth, half)
		font.spacewidth = half
	end
	
	font.glyphs = glyphs
	font.sane = true -- we just built this, so it should be good!
	
	devprint("buildFont success!")
	return true
end

local function fontSentinel(v)
	-- when called:
	-- - check font table for new additions and parse them
	-- - see if we have to regenerate any fonts for some reason
	
	for fontix = 1,#fonts do
		local font = fonts[fontix]
		if not font.sane then -- uh oh!
			devprint("insane font found. delegate > buildFont()")
			buildFont(v,font)
		end
	end
	
	return true
end

local function createNewFont(info)
	devprint("createNewFont called...")
	local font = {}
	
	font.info = info
	
	if type(info.prefix) ~= "string" then error("prefix must be a string",2) end
	if info.prefix:len() ~= 5 then error("prefix length must be exactly 5 characters",2) end
	font.prefix = info.prefix
	
	-- TODO maybe make some more checks to make sure font is somewhat good
	
	font.sane = false -- this will get fontSentinel to build the font
	fonts[#fonts+1] = font
	
	rawset(_G, "FONT_"..info.name, #fonts)
	devprint("createNewFont success!")
	
	return true
end


--##################
--## DRAWING TEXT ##
--##################

local flagval = 0; local function flagup() flagval = $+1; return 1<<(flagval-1) end
rawset(_G, "DF_INTPOS", flagup())
	-- indicates you're not using fixed point coords
	-- multiply coords by FRACUNIT
	
rawset(_G, "DF_MONOSPACE", flagup())
	-- if set, uses monospace
	
rawset(_G, "DF_ADVANCED", flagup())
	-- if set, enables more computationally expensive features
	-- TODO what?
	-- most useful for short strings
	
--rawset(_G, "DF_WHITESPACEGLYPH", flagup())
	-- if set, when parsing whitespace, place a fake glyph with no associated patch
	-- useless for normal use, but if you do something that counts characters drawn,
	-- might be useful
	
rawset(_G, "DF_WORDWRAP", flagup())
	-- if wrapping is enabled, this flag changes wrapping behavior
	-- by attempting to avoid breaking words.
	
rawset(_G, "DF_STRICT", flagup())
	-- if the drawer attempts to use a glyph that does not have a graphical representation,
	-- and it is not a character with special interpretation (\n, space, ???),
	-- raise error

--rawset(_G, "DF_SPECIALGLYPHS", flagup())
	-- if enabled, special glyphs (space, newline...) will draw their respective glyph.
	-- caveats:
	-- - if there is a space glyph, spacewidth is ignored
	-- - a newline glyph will be drawn, and line will shift afterwards
	-- nah on second thought this is stupid and useless, just use the other 255 chars

local function prepareLine()
	return {
		x = 0,
		y = 0,
		width = 0,
		height = 0,
		lastWordStart = 0, -- ix of last word char preceded by whitespace. 0 if this has not happened yet
	}
end
local function concludeLine(board, newline)
	local line = board.workline
	board[#board+1] = line
	-- TODO: calculate line width and offset, mayhaps
	
	--line.x = FixedMul(line.curwidth, FU - board.halign)
	--line.x = line.curwidth
	line.x = 0 -- for now
	
	local lastPiece = line[#line]
	line.width = lastPiece.x + lastPiece.width -- sensible
	
	--board.widestLineWidth = max(line.width, $)
	board.autowidth = max(line.width, $)
	board.autoheight = $ + line.height
	
	if newline then
		-- line completed, perform final adjustments
		local nextline = prepareLine()
		nextline.y = line.y + line.height -- move this new line downwards a bit, ya?
		board.workline = nextline
		return nextline
	else board.workline = nil end
end
local function makePiece(ascii)
end
local function makeBoard(drawdata)
	-- takes a table with text and formatting detail
	-- outputs a "board" of pieces (or patches) to be drawn
	
	local board = {
		x = 0,
		y = 0,
		width = drawdata.width or -1,
		height = -1,
		autowidth = 0,
		autoheight = 0,
	}
	local str = drawdata.text
	local font = fonts[drawdata.font] -- TODO check this exists before using
	
	local crsr = 0 -- current character from the string being processed. 1 is first char
	local crsrMax = #str -- when to stop
	
	local strict = drawdata.wflags & DF_STRICT > 0 -- strict mode (missing glyphs raise error)
	local advanced = drawdata.wflags & DF_ADVANCED > 0 -- advanced mode (cool stuff)
	
	local monospaceMode = drawdata.wflags & DF_MONOSPACE > 0 -- advanced mode (cool stuff)
	local wordWrap = drawdata.wflags & DF_WORDWRAP > 0 -- word wrapping is enabled
	local widthLimit = drawdata.width ~= nil -- glyphs should be bound within box
	
	--local curchr = 1 -- 
	--local curheight = 0 --
	
	board.workline = prepareLine() -- current line being worked on
	
	-- begin parsing every character into a drawable patch
	-- TODO should keep track of last word character for DF_WORDWRAP to be able to work
	while crsr < crsrMax do
		crsr = $+1; local chr = str:byte(crsr,crsr) -- advance cursor and extract a char as byte
		local line = board.workline
		
		local notFirstSpace = (#line > 0 and font.sepwidth*FRACUNIT or 0)
		-- add a little extra space if this is not the first character and sepwidth is nonzero
		
		local piece = {
			-- these positions should be relative to line start pos
			x = line.width + notFirstSpace, -- start at the right side of the last piece + separator
			y = 0, -- should always be zero :shrug:
			width = 0, -- "sensible" default
			height = 0, -- same here
			hscale = drawdata.hscale,
			vscale = drawdata.vscale,
		}
		
		local finishLine = false
		-- if this char is special, parse special properties first
		-- TODO tabulation
		if chr == 0x20 then -- " ", space
			-- special: just add distance, do not draw. drop piece
			piece.whitespace = true
			piece.width = FixedMul(font.spacewidth * FRACUNIT, piece.hscale) -- piece gets dropped anyways, might change later
			
			-- TODO unless it has an associated patch OR specific flag is set.
			
		elseif chr == 0x0A then -- "\n", line feed
			piece.whitespace = true
			finishLine = true
			
		else -- everyday piece
			local glyph = font.glyphs[chr] -- a glyph from the font
			piece.glyph = glyph
			if glyph.exists then
				piece.width = FixedMul((glyph.width) * FRACUNIT, piece.hscale)
				piece.height = FixedMul((glyph.height) * FRACUNIT, piece.vscale)
			end
		end
		
		if monospaceMode then
			piece.width = FixedMul((font.monowidth) * FRACUNIT, piece.hscale)
			--piece.height = (glyph.height) * FRACUNIT -- :shrug:
		end
		
		local totalwidth = notFirstSpace + piece.width
		
		if not piece.whitespace then
			if widthLimit then -- if board has a width limit. see if we can fit piece first
				if line.width + totalwidth > board.width then
					-- this character would go past bounds. que for next line...
					if wordWrap then
						-- TODO solution is ugly
						-- look for an alternative
						-- commit what i have
					else
						line = concludeLine(board, true)
						piece.x = 0 -- congrats on the promotion, buddy!
						notFirstSpace = 0 -- null space bonus, we started a new line
					end
				end
			end
			
			line[#line+1] = piece -- place piece in line
		else
			line.lastWordStart = #line -- get marked buddy
		end
		
		line.width = $ + notFirstSpace + piece.width -- this line has become wider
		line.height = max($, piece.height) -- lol.
		
		if finishLine then concludeLine(board, true) end
	end -- no more characters
	
	-- if there was a line in progress, complete it
	concludeLine(board, false)
	
	-- set board bounds if not set by drawdata
	if drawdata.width == nil then board.width = board.autowidth end
	if drawdata.height == nil then board.height = board.autoheight end
	
	-- align lines now
	-- TODO: if boards have variable height, consider that. for now, ignore
	local effhalign = (drawdata.halign/2) + FRACUNIT/2 -- f(x) = 0.5x + 0.5
	local effvalign = (drawdata.valign/2) + FRACUNIT/2
	
	-- align lines horizontally, then again for board
	for lineix = 1,#board do
		local line = board[lineix]
		local halignpad = FixedMul(board.width - line.width, effhalign)
		--local valignpad = FixedMul(board.width - line.height, effvalign)
		line.x = $ + halignpad
	end
	board.x = FixedMul(-board.width, effhalign)
	board.y = FixedMul(-board.height, effvalign)
	
	return board -- drawBoard would use this
end

local function co_boardIterator(board)
	
	for lineix = 1,#board do
		local line = board[lineix]
		for crsr = 1,#line do
			local piece = line[crsr]
			
			coroutine.yield(piece, line)
		end
	end
	
end

local function drawBoard(v, board, drawdata)
	-- takes a "board" of patches that can be drawn
	-- and draws them
	
	local corou = coroutine.create(co_boardIterator)
	for piece,line in function()
		-- iterate pieces, returning each piece and respective line
		local code,piece,line = coroutine.resume(corou, board)
		return piece,line
	end do
		local finalx,finaly =
			drawdata.x + board.x + line.x + piece.x,
			drawdata.y + board.y + line.y + piece.y
		;
		v.drawStretched(
			finalx, finaly,
			piece.hscale, piece.vscale,
			piece.glyph.patch,
			drawdata.dflags, drawdata.colormap -- TODO: piece flags and piece colormap?
		)
	end
	
end

local function writeText(v, drawdata)
	fontSentinel(v) -- ensure fonts are ready to go
	
	-- should probably do some checks here to make sure drawdata is good
	drawdata.wflags = $ or 0
	if drawdata.scale == nil then drawdata.scale = FRACUNIT end
	if drawdata.hscale == nil then drawdata.hscale = drawdata.scale end
	if drawdata.vscale == nil then drawdata.vscale = drawdata.scale end
	
	if drawdata.halign == nil then drawdata.halign = -FRACUNIT end
	if drawdata.valign == nil then drawdata.valign = -FRACUNIT end
	
	drawdata.dflags = 0
	
	local intpos = drawdata.wflags & DF_INTPOS > 0 -- advanced mode (cool stuff)
	if intpos then
		drawdata.x = $ * FRACUNIT
		drawdata.y = $ * FRACUNIT
	end
	
	local board = makeBoard(drawdata) -- prepare the board
	drawBoard(v, board, drawdata) -- draw it to the screen
end

WTXT.writeText = writeText
WTXT.createNewFont = createNewFont

rawset(_G, "WTXT", WTXT)

devprint("I'm ready!")










