local gamestart = getTimeMicros()
hud.disable("score")
hud.disable("time")
hud.disable("rings")
hud.disable("lives")
hud.disable("stagetitle")

WTXT.createNewFont({
	name = "CONSOLECOLOR",
	prefix = "COLFN"
})

local loremipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus feugiat nunc ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean at tortor et nisl accumsan commodo vel ac sem. Donec quis eleifend ex. Mauris sit amet ligula eu nulla pharetra rutrum. Ut ultricies blandit ligula ut tincidunt. Pellentesque et nisl a sem pretium venenatis sit amet ut arcu. Nam faucibus odio nulla, at molestie nunc sodales vel. Vivamus sed libero in ante scelerisque lobortis ut vel sem."

local page = 1
local pages = {


{"general text writing", function(v,p)
	WTXT.writeText(v, {
		x = 0, y = 0,
		font = FONT_CONSOLECOLOR,
		text = "0,0"
	})
	WTXT.writeText(v, {
		x = 0, y = 8*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		text = "0,8"
	})
	WTXT.writeText(v, {
		x = 0, y = 16*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		text = "0,16"
	})
	
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 40*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		text = "single line, hii!!!"
	})

	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 60*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		text = "multi line\ncoming up.\nhello!"
	})
	
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 100*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		dflags = DF_MONOSPACE,
		text = "I AM ROBOT lol\nmonospace stuff\n11 23 45 123"
	})
	
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 130*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_RED),
		text = "this should be red"
	})
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 138*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_WHITE),
		text = "this should be white"
	})
end},
{"scale", function(v,p)
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 30*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		text = "no squish\nhellohello"
	})
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 50*FRACUNIT,
		hscale = FRACUNIT/2,
		font = FONT_CONSOLECOLOR,
		text = "horiz squish\nhellohello"
	})
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 70*FRACUNIT,
		vscale = FRACUNIT/2,
		font = FONT_CONSOLECOLOR,
		text = "verti squish\nhellohello"
	})
	WTXT.writeText(v, {
		x = 4*FRACUNIT, y = 90*FRACUNIT,
		--hscale = FRACUNIT/2, vscale = FRACUNIT/2,
		scale = FRACUNIT/2,
		font = FONT_CONSOLECOLOR,
		text = "both squish\nhellohello"
	})
	
	/*
	// getTimeMicros wraps around every 72 minutes. time start is not synced to game start!!!
	// do not use getTimeMicros outside profiling
	local microsNow = getTimeMicros() - gamestart
	local millisNow = microsNow/1000
	print(millisNow)
	
	local magic = 1000/35
	local millisNowButCool = millisNow/magic
	
	local hfuny = cos(FixedAngle(millisNowButCool*5*FRACUNIT))/2
	local vfuny = cos(FixedAngle(millisNowButCool*7*FRACUNIT))/2
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 40*FRACUNIT,
		hscale = FRACUNIT+hfuny, vscale = FRACUNIT+vfuny,
		font = FONT_CONSOLECOLOR,
		text = "awesome it's\ngetTimeMicros()\n"..millisNow
	})
	-- turns out the hud is polled at TICRATE fps
	-- so this serves no purpose.
	*/
	
	local hfuny = cos(FixedAngle(leveltime*5*FRACUNIT))/2
	local vfuny = cos(FixedAngle(leveltime*7*FRACUNIT))/2
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 90*FRACUNIT,
		hscale = FRACUNIT+hfuny, vscale = FRACUNIT+vfuny,
		font = FONT_CONSOLECOLOR,
		text = "boioioing\noiong\n"..leveltime
	})
end},
{"alignment", function(v,p)
	
	WTXT.writeText(v, {
		x = 6*FRACUNIT, y = 6*FRACUNIT,
		halign = 0, valign = 0,
		font = FONT_CONSOLECOLOR,
		text = "mein gott!!\ni hath\na centerth!"
	})
	
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 100*FRACUNIT,
		halign = -FRACUNIT, valign = -FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_RED),
		text = "TOP\nLEFT"
	})
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 100*FRACUNIT,
		halign = FRACUNIT, valign = -FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_YELLOW),
		text = "TOP\nRIGHT"
	})
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 100*FRACUNIT,
		halign = -FRACUNIT, valign = FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_GREEN),
		text = "BOTTOM\nLEFT"
	})
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 100*FRACUNIT,
		halign = FRACUNIT, valign = FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_BLUE),
		text = "BOTTOM\nRIGHT"
	})
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 100*FRACUNIT,
		halign = 0, valign = 0,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_BLACK),
		text = "X"
	})
end},

{"wrap tests", function(v,p)
	WTXT.writeText(v, {
		x = 40*FRACUNIT, y = 12*FRACUNIT,
		--width = 80*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_RED),
		text = "No word wrap"
	})
	WTXT.writeText(v, {
		x = 40*FRACUNIT, y = 20*FRACUNIT,
		width = 80*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		dflags = 0,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_YELLOW),
		text = loremipsum
	})
	
	WTXT.writeText(v, {
		x = 200*FRACUNIT, y = 12*FRACUNIT,
		--width = 80*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_GREEN),
		text = "Word wrap"
	})
	WTXT.writeText(v, {
		x = 200*FRACUNIT, y = 20*FRACUNIT,
		width = 80*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		dflags = DF_WORDWRAP,
		colormap = v.getColormap(TC_DEFAULT, SKINCOLOR_YELLOW),
		text = loremipsum
	})
	
end},

{"ctrl+c ctrl+v me", function(v,p)
	WTXT.writeText(v, {
		x = 160*FRACUNIT, y = 90*FRACUNIT,
		font = FONT_CONSOLECOLOR,
		text = "text here"
	})
end},


}

addHook("KeyDown", function(e)
	if e.name == "a" then page = $-1 end
	if e.name == "d" then page = $+1 end
	--print(e.name)
	while page > #pages do page = $ - #pages end
	while page < 1 do page = $ + #pages end
end)

hud.add(function(v,p,c)
	
	local selpage = pages[page]
	selpage[2](v,p)
	
	v.drawString(0,200-8,
		("%d/%d - %s"):format(page, #pages, selpage[1]),
		V_SNAPTOBOTTOM|V_SNAPTOLEFT|V_ALLOWLOWERCASE, "thin")
	
	-- ok
end)