intended flow:

writetext(v, drawdata) {
	fontSentinel is called {
		build fonts if needed
		should not take long
	}
	
	board = bakeboard(drawdata) {
		take the string to draw and split it into individual glyphs
		interpret every character and preparse() and translate to glyph or action
		separate into lines if required
		position every individual glyph, taking align and scale and glyph size into account
		calculate line widths for alignment
	}
	
	drawboard(board) {
		read the board line by line
		predraw()
		draw every glyph at the specified position with specified scale... and other
		postdraw()
	}
}
